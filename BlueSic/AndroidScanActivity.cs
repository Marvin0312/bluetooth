﻿using System;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace BlueSic
{
    [Activity(Label = "@string/select_device",
                Theme = "@android:style/Theme.Holo.Dialog",
                ConfigurationChanges = Android.Content.PM.ConfigChanges.KeyboardHidden | 
                Android.Content.PM.ConfigChanges.Orientation)]
    class AndroidScanActivity : Activity
    {
        private BluetoothAdapter btAdapter;
        private static ArrayAdapter<string> newDevicesArrayAdapter;
        private DeviceDiscoveredReceiver receiver;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_android_scan);
            SetResult(Result.Canceled);

            btAdapter = BluetoothAdapter.DefaultAdapter;
            
            ///define intentFilter for bluetooth search event 
            receiver = new DeviceDiscoveredReceiver(this);
            var filter = new IntentFilter(BluetoothDevice.ActionFound);
            RegisterReceiver(receiver, filter);
            filter = new IntentFilter(BluetoothAdapter.ActionDiscoveryFinished);
            RegisterReceiver(receiver, filter);

            ///adapter for already paired devices
            var pairedDevicesArrayAdapter = new ArrayAdapter<string>(this, Resource.Layout.device_name);
            var pairedListView = FindViewById<ListView>(Resource.Id.paired_devices);
            pairedListView.Adapter = pairedDevicesArrayAdapter;
            pairedListView.ItemClick += DeviceListView_ItemClick;
            var pairedDevices = btAdapter.BondedDevices;

            if (pairedDevices.Count > 0) {
                FindViewById(Resource.Id.title_paired_devices).Visibility = ViewStates.Visible;
                foreach (var device in pairedDevices)
                    pairedDevicesArrayAdapter.Add(device.Name + "\n" + device.Address);
            }
            else {
                var noDevices = Resources.GetText(Resource.String.none_paired);
                pairedDevicesArrayAdapter.Add(noDevices);
            }

            ///adapter for new devices
            newDevicesArrayAdapter = new ArrayAdapter<string>(this, Resource.Layout.device_name);
            var newDevicesListView = FindViewById<ListView>(Resource.Id.new_devices);
            newDevicesListView.Adapter = newDevicesArrayAdapter;
            newDevicesListView.ItemClick += DeviceListView_ItemClick;

            ///define onClick event
            var scanButton = FindViewById<Button>(Resource.Id.button_scan);
            scanButton.Click += delegate {
                //this.setSupportProgressBarIndeterminateVisibility(true);
                newDevicesArrayAdapter.Clear();
                scanButton.Visibility = ViewStates.Gone;
                // If we're already discovering, stop it
                if (btAdapter.IsDiscovering)
                    btAdapter.CancelDiscovery();

                btAdapter.StartDiscovery();
            };
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            // Make sure we're not doing discovery anymore
            if (btAdapter != null)
                btAdapter.CancelDiscovery();

            // Unregister broadcast listeners
            UnregisterReceiver(receiver);
        }

        private void DeviceListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            btAdapter.CancelDiscovery();

            // Get the device MAC address, which is the last 17 chars in the View
            var info = ((TextView)e.View).Text;
            var address = info.Substring(info.Length - 17);

            // Create the result intent and include MAC address.
            var intent = new Intent();
            intent.PutExtra("device_address", address);
            SetResult(Result.Ok, intent);
            Finish();
        }

        public class DeviceDiscoveredReceiver : BroadcastReceiver
        {
            private Activity chatActivity;

            public DeviceDiscoveredReceiver(Activity chat)
            {
                this.chatActivity = chat;
            }

            public override void OnReceive(Context context, Intent intent)
            {
                string action = intent.Action;

                // When discovery finds a device
                if (action == BluetoothDevice.ActionFound)
                {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
                    // If it's already paired, skip it, because it's been listed already
                    if (device.BondState != Bond.Bonded)
                        newDevicesArrayAdapter.Add(device.Name + "\n" + device.Address);
                }
                else if (action == BluetoothAdapter.ActionDiscoveryFinished)
                {
                    chatActivity.FindViewById<Button>(Resource.Id.button_scan).Visibility = ViewStates.Visible;
                    //chatActivity.Sup;
                    if (newDevicesArrayAdapter.Count == 0) {
                        var noDevices = chatActivity.Resources.GetText(Resource.String.none_found).ToString();
                        newDevicesArrayAdapter.Add(noDevices);
                    }
                }
            }
        }
    }
}