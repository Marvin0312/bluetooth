﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace BlueSic
{
    public class ChatFragment : Fragment
    {
        const string TAG = "BlueSic.ChatFragment";
        const int ENABLE_BT = 1;

        public ChatFragment (BluetoothAdapter blue)
        {

        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            
            return inflater.Inflate(Resource.Layout.chat_fragment, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            var button = view.FindViewById<Button>(Resource.Id.buttonSearch);
            button.Click += delegate {
                var intent = new Intent(Activity, typeof(AndroidScanActivity));
                StartActivityForResult(intent, 1);
            };
        }

        public override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            switch (requestCode)
            {
                case 1:
                    if (Result.Ok == resultCode)
                    {
                        Toast.MakeText(Activity, "ok", ToastLength.Long).Show();
                    } else if (Result.Canceled == resultCode)
                    {
                        Toast.MakeText(Activity, "cancel", ToastLength.Long).Show();
                    }
                    break;
            }
        }
    }
}