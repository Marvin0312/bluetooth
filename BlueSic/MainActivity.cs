﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Bluetooth;
using Android.Content;
using System.Collections;
using System;

namespace BlueSic
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        const string TAG = "BlueSic.MainActivity";
        BluetoothAdapter blue;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            blue = BluetoothAdapter.DefaultAdapter;
            Intent i = new Intent(BluetoothAdapter.ActionRequestEnable);
            StartActivityForResult(i, 0);

            i = new Intent(BluetoothAdapter.ActionRequestDiscoverable);
            StartActivityForResult(i, 0);

            FindViewById<Button>(Resource.Id.buttonHost).Click += ButtonServer_OnClick;
            FindViewById<Button>(Resource.Id.buttonJoin).Click += ButtonJoin_OnClick;
        }

        private void ButtonServer_OnClick(object sender, EventArgs e)
        {
            StartActivity(typeof(ServerActivity));
        }

        private void ButtonJoin_OnClick(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(AndroidScanActivity));
            StartActivityForResult(intent, 1);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.changeBlStatus:
                    if (blue.Enable()) {
                        blue.Disable();
                        Toast.MakeText(this, "Bluetooth Disabled", ToastLength.Short).Show();
                    } else {
                        Intent i = new Intent(BluetoothAdapter.ActionRequestEnable);
                        StartActivityForResult(i, 0);
                        Toast.MakeText(this, "Enabled", ToastLength.Short).Show();
                    }
                    return true;

                case Resource.Id.visible:
                    Intent visible = new Intent(BluetoothAdapter.ActionRequestDiscoverable);
                    StartActivityForResult(visible, 0);
                    return true;

                case Resource.Id.pairDevices:
                    
                    return true;

                case Resource.Id.appOptions:
                    /*
                     * okodować ustawienia
                     */
                    return true;
            }
            return false;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            switch (requestCode)
            {
                case 1:
                    if (Result.Ok == resultCode)
                    {
                        Toast.MakeText(this, "ok", ToastLength.Long).Show();
                    }
                    else if (Result.Canceled == resultCode)
                    {
                        Toast.MakeText(this, "cancel", ToastLength.Long).Show();
                    }
                    break;
            }
        }
    }
}

